// Теоретичні питання

// 1. Які існують типи даних у Javascript?

// JavaScript має 8 основних типів: number, bigInt, string, boolean, null, undefined, object, typeof.

// 2. У чому різниця між == і ===?

// == Повертає true, лише якщо два операнди рівні, тоді як === повертає true, лише якщо значення та типи даних однакові для двох змінних.

// 3. Що таке оператор?

// Оператори служать для управління потоком команд JavaScript. Один об'єкт може бути розбитий на кілька рядків, або, навпаки, в одному рядку може бути кілька операторів.

// Завдання

function userVerification() {
    let userName = prompt('Як вас звати?');

    while (!userName) {
        userName = prompt('Як вас звати?', userName);
    }

    let userAge = null

    while (!userAge || isNaN(userAge)) {
        userAge = parseInt(prompt('Скільки вам років?', userAge));
    }

    if (userAge < 18) {
        alert('You are not allowed to visit this website');
    }
    else if (userAge <= 22) {
        let allow = confirm('Are you sure you want to continue?');
        if (allow === true) {
            alert(`Welcome, ${userName}`);
        } else {
            alert('You are not allowed to visit this website');
        }

    } else {
        alert(`Welcome, ${userName}`);
    }
    console.log(`name: ${userName}, userAge: ${userAge}`);
}
userVerification();